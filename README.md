# Wireguard Subspace

Wireguard installer with Subspace web interface

## Usage

Run the script and follow the assistant:
```
wget https://gitlab.com/isystem-io/products/infrastructure/wireguard-subspace/raw/master/install.sh -O ws-install.sh
bash ws-install.sh
```

#TODO: Нарисовать блок-схему скрипта