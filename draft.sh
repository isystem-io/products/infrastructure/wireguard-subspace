
## Перебор параметров с ключами
#!/bin/bash
while [ -n "$1" ]
do
case "$1" in
-a) echo "Found the -a option";;
-b) param="$2"
echo "Found the -b option, with parameter value $param"
shift ;;
-c) echo "Found the -c option";;
--) shift
break ;;
*) echo "$1 is not an option";;
esac
shift
done
count=1
for param in "$@"
do
echo "Parameter #$count: $param"
count=$(( $count + 1 ))
done


#key=value
for argument in ${@}; do
    case $argument in
        -k=* | --key=* )
            VALUE=${argument##*=} # Запишет значение после знака равно в переменную VALUE
            ;;
    esac
done








function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

A=("y" "Y" "n" "N")

if [ $(contains "${A[@]}" "y") == "y" ]; then
    echo "contains one"
fi
if [ $(contains "${A[@]}" "yN") == "y" ]; then
    echo "contains three"
fi


containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

array=("y" "Y" "n" "N")
containsElement "y" "${array[@]}"

containsElement () { 
    answers=(y Y n N)
    for e in "${@:2}"; do [[ "$e" = "$1" ]] && return 0; done; return 1; 
    }


[[" ${array[@]} " =~ " ${value} "]]

find_in_array() {
  local word=$1
  shift
  for e in "$@"; do [[ "$e" == "$word" ]] && return 0; done
}

containsElement () { 
    answers=(y Y n N)
    
    for e in "${@:2}"; do [[ "$e" = "$1" ]] && return 0; done; return 1; 
    }

some_words=( y Y n N )
containsElement y "${some_words[@]}" || echo "expected missing! since words != y"
containsElement Y "${some_words[@]}" || echo "expected missing! since words != Y"
containsElement n "${some_words[@]}" || echo "expected missing! since words != n"
containsElement N "${some_words[@]}" || echo "expected missing! since words != N"
containsElement yN "${some_words[@]}" || echo "expected missing! since words != yN"
containsElement Yn "${some_words[@]}" || echo "expected missing! since words != Yn"




awk -F. '{print $1}' /etc/passwd
 echo "My name is Tom" | awk '{$4="Adam"; print $0}'

echo "vpn.local" | awk -F. '{print $1; print $0}'

select varName in list
do
    command1
    command2
    ....
    ......
    commandN
done

#!/bin/bash
# The default value for PS3 is set to #?.
# Change it i.e. Set PS3 prompt
PS3="Enter the space shuttle to get quick information : "
 
# set shuttle list
select shuttle in columbia endeavour challenger discovery atlantis enterprise pathfinder
do
	case $shuttle in
		columbia)
			echo "--------------"
			echo "Space Shuttle Columbia was the first spaceworthy space shuttle in NASA's orbital fleet."
			echo "--------------"
			;;
		endeavour)
			echo "--------------"		
			echo "Space Shuttle Endeavour is one of three currently operational orbiters in the Space Shuttle." 
			echo "--------------"		
			;;
		challenger) 
			echo "--------------"				
		    echo "Space Shuttle Challenger was NASA's second Space Shuttle orbiter to be put into service."
			echo "--------------"				    
			;;		
		discovery) 
			echo "--------------"		
			echo "Discovery became the third operational orbiter, and is now the oldest one in service."
			echo "--------------"							
			;;		
		atlantis)
			echo "--------------"		
			echo "Atlantis was the fourth operational shuttle built."
			echo "--------------"							
			;;
		enterprise)
			echo "--------------"		
			echo "Space Shuttle Enterprise was the first Space Shuttle orbiter."
			echo "--------------"							
			;;		
		pathfinder)
			echo "--------------"		
			echo "Space Shuttle Orbiter Pathfinder is a Space Shuttle simulator made of steel and wood."
			echo "--------------"							
			;;
		*)		
			echo "Error: Please try again (select 1..7)!"
			;;		
	esac
done