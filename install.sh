#!/bin/bash

# Checking Prerequisites
if [[ "$EUID" -ne 0 ]]; then
    echo "Sorry, you need to run this as root"
    exit 1
fi

# Get the name of the Linux distribution
if [ -e /etc/debian_version ]; then
    DISTRO=$( lsb_release -is )
elif [ -e /etc/centos-release ]; then
    DISTRO="CentOS"
else
    DISTRO="Unknow"
fi

# So far, only for Debian will the installer work
if [ "$DISTRO" != "Debian" ]; then
# elif [ "$DISTRO" == "Ubuntu" ]; then
# elif [ "$DISTRO" == "CentOS" ]; then
# else 
    echo "Your distribution is not supported (yet)"
    exit 1
fi

# Array of answers
ynAnswers=("y" "Y" "n" "N")

checkAnswer () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && echo 0 && return 0; done
  echo 1 && return 1
}

# checkAnswerWithNull () {
#   local e match="$1"
#   shift
#   for e; do [[ "$e" == "$match" || "$match" == ""]] && echo 0 && return 0; done
#   echo 1 && return 1
# }   

localHostname='';
newUserName='';
userPassword='';

#key=value
for argument in ${@}; do
    case $argument in
        -l=* | --localhostname=* )
            localHostname=${argument##*=}
            ;;
        -u=* | --username=* )
            newUserName=${argument##*=}
            ;;
        -p=* | --password=* )
            userPassword=${argument##*=}
            ;;

        # -k=* | --key=* )
        #     VALUE=${argument##*=}
        #     ;;
        # -k=* | --key=* )
        #     VALUE=${argument##*=}
        #     ;;
        # -k=* | --key=* )
        #     VALUE=${argument##*=}
        #     ;;

    esac
done

echo "=================================="
echo "=== Install Wireguard&Subspace ==="
echo "=================================="

echo "=== Preparation for installation ==="

# update & upgrade os
apt update -y
apt dist-upgrade -y

# install common tools
apt install sudo wget git curl screen -y #apache2-utils 

# change hostname
defaultLocalHostName='vpn.local';
answerChangeLocalHostname='';
answerSetLocalHostname='';

function setHostNameDebian(){ #$1 localHostname $2 smallLocalHostname
    hostnamectl set-hostname $1

    #TODO при добавлении удалять старые записи 
    echo "127.0.0.1      $1 $2" >> /etc/hosts
    echo "::1     $1 $2" >> /etc/hosts
    return 0;
}

function setHostName(){ #$1 localHostname
    local smallLocalHostname=$(echo $1 | awk -F. '{print $1;}')
    setHostNameDebian $1 $smallLocalHostname && echo "Local hostname changed to $1"
}

if [ -z  "$localHostname" ]
then 
    while [ $(checkAnswer "$answerSetLocalHostname" "${ynAnswers[@]}") != 0 ]
    do
        read -p "Do you want to change the local host name? [y/n]: " answerSetLocalHostname;
    done

    case $answerSetLocalHostname in
        y|Y)
            read -p "Enter the local hostname as FQDN ([ENTER] set to default: $defaultLocalHostName): " localHostname
            if [ -z $localHostname ]
            then localHostname=$defaultLocalHostName
            fi
            setHostName $localHostname
            ;;
        n|N|'')
            ;;
    esac
else setHostName $localHostname;
fi

# add vpn user
defaultNewUserName='creator';
answerNewUserName='';

if [ -z  "$newUserName" ]
then 
    while [ $(checkAnswer "$answerNewUserName" "${ynAnswers[@]}") != 0 ]
    do
        read -p "Want to add a new user? [y/n]: " answerNewUserName;
    done

    case $answerNewUserName in
        y|Y)
            read -p "Enter new username ([ENTER] set to default: $defaultNewUserName): " newUserName
            if [ -z $newUserName ]
            then newUserName=$defaultNewUserName
            fi
            addNewUser $newUserName
            ;;
        n|N|'')
            ;;
    esac
else addNewUser $newUserName;
fi

#Добавлять поьзователя? да нет

adduser creator
usermod -aG sudo creator
groups creator
## Если добавлять, тогда запрос имени, или по дефолту. Запрос пароля. 
### Генерировать ключи, или использовать существующие.
### настойка сервера ssh.

# Настройка git.
# Уставновка docker & docker-compose.
# Установить Wieguard
# Настройка gpg
# Настройка почтового сервера

